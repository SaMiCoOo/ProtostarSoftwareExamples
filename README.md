# Project 1: Software Security

I tried to use Valgrind but it never started and always threw this error 


```
$ Warning: Can't execute setuid/setgid executable: ./stack0
$ Possible workaround: remove --trace-children=yes if in effect
$ valgrind: ./stack0: Permission denied
```

I tried the workaround mentioned in the error log, but it was useless here, also tried to check the permission of valgrind and the executables and still did not work.


So instead I kept using GDB to print for debugging.

### Bookmarks
* [stack0 solution](#stack0)
* [stack1 solution](#stack1)
* [format0 solution](#format0)
* [format1 solution](#format1)
* [Questions](#questions)

--
 
## Solution
### stack0
This level introduces the concept that memory can be accessed outside of its allocated region, how the stack variables are laid out, and that modifying outside of the allocated memory can modify program execution.

```c
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char **argv)
{
  volatile int modified;
  char buffer[64];

  modified = 0;
  gets(buffer);

  if(modified != 0) {
      printf("you have changed the 'modified' variable\n");
  } else {
      printf("Try again?\n");
  }
}
```

list of all variables inside the main function.

|Variable|Offset|Value |Reserved Size|
|---|---|---|---|
|modified|?|0|4 Bytes|
|buffer|?|user input|64 Bytes|

Since the input from the user is not controlled to a maximum number of characters the variable **buffer** which is **64Bytes** chars will overflow the data section of the code where the other variable **modified** is stored and if more than the sum of both variables size, which is **68Bytes** was input by the user the program stack will be overflown and most likely crash.

To make sure that both variables are indeed saved on top of each other in the stack I used the gdb `info address [SYMBOL]` command to get both variables addresses offset from the main function.

|Variable|Offset|Value |Reserved Size|
|---|---|---|---|
|modified|**92**|0|4 Bytes|
|buffer|**28**|user input|64 Bytes|

And indeed both variables were saved successively in the stack since the offset of **modified** is equal to offset of **buffer** + **SIZEOF(buffer)** = 28 + 64 = 92.

After making sure that both variables are saved successively in the stack all is needed now is to enter an `input of size > 64` to overflow **modified** variable but also `<=68` to make sure the program does not crash due to a stack overflow error.

![Stack0](stack0.png)

Here is the program given the input of size **65Bytes** which changed the **modified** variable value changed, and so the line below executed and printed out `you have changed the 'modified' variable`

```c
if(modified != 0) {
      printf("you have changed the 'modified' variable\n");
  }
```

#### Using GDB to look at the stack before and after the modifing the variables:

since the **buffer** have offset **28Bytes** from the **esp** pointer and **modified** is **96Bytes** then by printing the stack from **esp** + 96Bytes will show us both variables we are interested in.
And since **GDB** prints in increments with *dword* then we will write the offset 96/4=24

![stack0-2](stack0-2.png)

##### This is the output of `x/24x $esp` before executing `gets()`

The first register printed is *0xbffffcc00* which is the esp, if we add to i the hex value of the offset of **28Bytes** (0x1C) we get the register which holds the first Char(Byte) of the **buffer** variable the next 63 Bytes are reserved for the **buffer**.

Following them are the 4 Bytes reserved and initialized by value **0** for the **modified** variable.


![stack0-3](stack0-3.png)

##### This is the output of `x/24x $esp` after executing `gets()`

After entering 64 "A" character which have ascii-code of (41), which is filling the the 64Bytes of the **buffer** variable, leaving the **modified** variable untouched.





![stack0-4](stack0-4.png)

##### This is the output of `x/24x $esp` after executing `gets()` and overflowing the **modified** variable.

now that we added 4 extra characters to the input it overflowed and filled the **modified** variable with the ascii code of the extra characters.


--

### stack1

This level looks at the concept of modifying variables to specific values in the program, and how the variables are laid out in memory.

Basically the same thing we just did with **stack0** example but this time we need to fill overflow the **modified** variable with the specified 4 Byte value **0x61626364**.

```c
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
  volatile int modified;
  char buffer[64];

  if(argc == 1) {
      errx(1, "please specify an argument\n");
  }

  modified = 0;
  strcpy(buffer, argv[1]);

  if(modified == 0x61626364) {
      printf("you have correctly got the variable to the right value\n");
  } else {
      printf("Try again, you got 0x%08x\n", modified);
  }
}
```

The first problem here is that the input data is saved in the stack as a **`char array`** the hexadecimal values stored is actually the ascii code of each character, and so reading the **modified** gives **`abcd`** string.

The second is that **`char array`** in intels' implementation is stored in little-endian method ( *Least Significant first* ) so the actual input will be byte index inverted.

```
SOME_64_CHAR_ARRAY followed by dcba
```


![stack1](stack1.png)
![stack1-2](stack1-2.png)
##### This is the output of `x/24x $esp` after executing `strcpy()` on line 18.

using printing out the **96 bytes** following the **esp** shows us how the stack got filled as expected and them modified variable is now equal to **0x61626364** as required.

---

### format0



```c
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

void vuln(char *string)
{
  volatile int target;
  char buffer[64];

  target = 0;

  sprintf(buffer, string);
  
  if(target == 0xdeadbeef) {
      printf("you have hit the target correctly :)\n");
  }
}

int main(int argc, char **argv)
{
  vuln(argv[1]);
}
```

Again this is very similar to the last example but the 4byte value is **0xdeadbeef**, but here we can insert the hexadecimal as it is, but still in a reveresed order so the input will be:

```
SOME_64_CHAR_ARRAY followed by \xef\xbe\xad\xde
```
#### Successful solution
![format0](format0.png)

#### Stack values from address ( ebp - 76 Byte ) to ( ebp - 8 Bytes)
`x/17x $ebp - 76`
![format0-2](format0-2.png)

The few differences here is that the **target** variable is inside a function called by the `main` function so the offset here calculated by **gdb** is from the `base pointer $ebp` inside the frame of `vuln()`.

|Variable|Offset|Value |Reserved Size|
|---|---|---|---|
|target|**-12**|0xdeadbeef|4 Bytes|
|buffer|**-76**|'A' <64 times>|64 Bytes|

Again since both variable are directly above each other in the stack every extra character inserted to the **buffer** variable will overflow in the **target** variable.

" I did not use the same method to print the needed variables above as I did not know the syntax of `x/17x $ebp - 76` was possible in **GDB**  at that time."




--

### format1

```c
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int target;

void vuln(char *string)
{
  printf(string);
  
  if(target) {
      printf("you have modified the target :)\n");
  }
}

int main(int argc, char **argv)
{
  vuln(argv[1]);
}
```

The problem here with the code is that it prints the input directly from `printf` function without defining that it is a string literal with the string formater `%s`.

To exploit this mistake we need to know **2** things:

1. The static reference of the variable **target** since it is not in the same frame as the input string so we can't overwrite **target**  variable with the overflow method as before without overwriting other frames code which will cause a segmentation error.
2. How much Byte padding is needed to overwrite the second argument of the `printf` address so that we can replace it with the static reference of **target** and then overwrite its value using the `%n` format character which writes the number of bytes given in the string into the second argument of the `format` function.


The static reference of **target** can be found using GDB function`info address [SYMBOL]`.

**`Static Reference of Target = 0x08049638`** 

reversing that into a string gives `"\x38\x96\x04\x08"`

The Byte padding can be calculated exactly using the GDB `ltrace` function and calculating the offset from the main destructor, but I choose to get get it through writing a string of known ascii code and keep trying multiple values until the string is the last thing is getting popped out by the %x formatter.

using this method I calculated the value to be **128Bytes** but when I swapped my string with the Static Reference I found out that I needed more padding, and I do not know why actually.

According to [Format String Exploitation-Tutorial](https://www.exploit-db.com/docs/28476.pdf) it should have worked.

but anyway after few trials it worked

![format1](format1.png)

---

## Questions

1. I am not really sure why `%n` overwrite the second argument of the `format` function by default, also I am not sure if that is even really true ? but at least that is what I understood from the paper [Format String Exploitation-Tutorial](https://www.exploit-db.com/docs/28476.pdf).
2. What is the best way to calcualte the needed **Byte Padding** in situations like **format1 example** ?
3. I read on how to write an exact value on a global variable using format vulnerabilty but I could not understand the multiple byte writing methods. Could this be discussed on campus ?
4. Why is **GDB** commands `info address` `info locals` and `display` does not work as intended in the scope of `main` function ?
5. Can we get a clear explanation of the **format1 example** next lecture?


